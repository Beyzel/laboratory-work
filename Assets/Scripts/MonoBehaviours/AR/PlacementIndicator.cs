using UnityEngine;

namespace Assets.Scripts.MonoBehaviours.AR
{
    public class PlacementIndicator : MonoBehaviour
    {
        [SerializeField] 
        private GameObject _point;

        private GameObject _newLab;

        public void InitializeLabPreview(GameObject currentLab)
        {
            _point.SetActive(false);
            _newLab = Instantiate(currentLab, transform);
        }

        public void DestroyLabPreview()
        {
            _point.SetActive(true);
            Destroy(_newLab);
        }
    }
}