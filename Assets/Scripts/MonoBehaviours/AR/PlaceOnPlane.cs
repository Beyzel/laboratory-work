using System.Collections.Generic;
using Assets.Scripts.MonoBehaviours.Controllers;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

namespace Assets.Scripts.MonoBehaviours.AR
{
    [RequireComponent(typeof(ARRaycastManager))]
    public class PlaceOnPlane : MonoBehaviour
    {
        [SerializeField] 
        private PlacementIndicator _placementIndicatorPrefab;
        [SerializeField]
        private ARSession _arSession;

        private ARRaycastManager _raycastManager;
        private ARSessionOrigin _arSessionOrigin;

        private GameObject _currentLab;

        private bool _isPlaced;
        private bool _isIndicatorInArea;
        
        private PlacementIndicator _placementIndicator;

        private void Awake()
        {
            InitializeArComponents();
            InitializeArMode();
        }

        private void Update()
        {
            SetIndicator();
        }

        private void OnEnable()
        {
            SubscribeOnEvents();
        }

        private void OnDisable()
        {
            UnSubscribeFromEvents();
        }
 
        private void SubscribeOnEvents()
        {
            LabController.SetCurrentLabEvent += SetCurrentLab;
            MenuController.ClearArLabPreviewEvent += ClearArLabPreview;
        }

        private void UnSubscribeFromEvents()
        {
            LabController.SetCurrentLabEvent -= SetCurrentLab;
            MenuController.ClearArLabPreviewEvent -= ClearArLabPreview;
        }

        private void SetCurrentLab(GameObject currentLab)
        {
            _currentLab = currentLab;
            SetLabPreview();
        }

        private void InitializeArMode()
        {
            _arSession.enabled = true;
            _arSessionOrigin.trackablesParent.gameObject.SetActive(true);
            _placementIndicator = Instantiate(_placementIndicatorPrefab);
        }

        private void InitializeArComponents()
        {
            _raycastManager = GetComponent<ARRaycastManager>();
            _arSessionOrigin = GetComponent<ARSessionOrigin>();
        }

        private void SetIndicator()
        {
            var screenCenter = Camera.main.ViewportToScreenPoint(new Vector3(0.5f, 0.5f));
            var hits = new List<ARRaycastHit>();

            if (_raycastManager.Raycast(screenCenter, hits, TrackableType.PlaneWithinPolygon) && !_isPlaced)
            {
                _placementIndicator.transform.position =  hits[0].pose.position;
                _isIndicatorInArea = true;
            }
        }

        private void SetLabPreview()
        {
            if (_isIndicatorInArea && _currentLab != null && !_isPlaced)
            {
                _placementIndicator.InitializeLabPreview(_currentLab);
                _arSessionOrigin.trackablesParent.gameObject.SetActive(false);
                _isPlaced = true;
            }
        }

        private void ClearArLabPreview()
        {
            _isPlaced = false;
            _isIndicatorInArea = false;
            _arSessionOrigin.trackablesParent.gameObject.SetActive(true);
            _currentLab = null;
            _placementIndicator.DestroyLabPreview();
        }
    }
}