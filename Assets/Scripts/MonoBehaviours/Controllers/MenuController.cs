using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.MonoBehaviours.Controllers
{
    public class MenuController : MonoBehaviour
    {
        public static event Action ClearArLabPreviewEvent;

        [SerializeField] 
        private Button _labsBtn;

        [SerializeField] 
        private GameObject _selection;

        private void OnEnable()
        {
            SubscribeOnEvents();
        }

        private void OnDisable()
        {
            UnsubscribeFromEvents();
        }

        private void SubscribeOnEvents()
        {
            _labsBtn.onClick.AddListener(ActivateSelectionMenu);
            LabController.LabBtnClickEvent += UpdateLab;
        }
        
        private void UnsubscribeFromEvents()
        {
            _labsBtn.onClick.RemoveListener(ActivateSelectionMenu);
            LabController.LabBtnClickEvent -= UpdateLab;
        }

        private void ActivateSelectionMenu()
        {
            _selection.SetActive(true);
            _labsBtn.gameObject.SetActive(false);
            ClearArLabPreviewEvent?.Invoke();
        }

        private void DeactivateSelectionMenu()
        {
            _selection.SetActive(false);
            _labsBtn.gameObject.SetActive(true);
        }
        private void UpdateLab()
        {
            DeactivateSelectionMenu();
        }
    }
}