using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.MonoBehaviours.Controllers
{
    public class LabController : MonoBehaviour
    {
        public static event Action LabBtnClickEvent;
        public static event Action<GameObject> SetCurrentLabEvent;

        [SerializeField]
        private GameObject _labPrefab;

        private Button _labButton;

        private void Awake()
        {
            _labButton = GetComponent<Button>();
        }

        private void OnEnable()
        {
            SubscribeOnEvents();
        }

        private void OnDisable()
        {
            UnsubscribeFromEvents();
        }

        private void SubscribeOnEvents()
        {
            _labButton.onClick.AddListener(SetLab);
        }

        private void UnsubscribeFromEvents()
        {
            _labButton.onClick.RemoveListener(SetLab);
        }

        private void SetLab()
        {
            LabBtnClickEvent?.Invoke();
            SetCurrentLabEvent?.Invoke(_labPrefab);
        }
    }
}