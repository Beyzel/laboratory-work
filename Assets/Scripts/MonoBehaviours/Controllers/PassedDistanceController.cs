using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts.MonoBehaviours.Controllers
{
    public class PassedDistanceController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        public static event Action<float> SliderChangedValueEvent;
        private Slider _passedDistance;

        private bool _slide;
        
        public bool Slide => _slide;

        private void Start()
        {
            _passedDistance = GetComponent<Slider>();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            _slide = true;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            SliderChangedValueEvent?.Invoke(_passedDistance.value);
            _slide = false;
        }

        public void SetSliderValue(float value)
        {
            _passedDistance.value = value;
        }
    }
}
