using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts.MonoBehaviours.Controllers
{
    public class ControllButtons : MonoBehaviour
    {
        public static event Action PlayBtnClickEvent;
        public static event Action PauseBtnClickEvent;
        public static event Action SpeedUpBtnClickEvent;
        public static event Action SpeedDownBtnClickEvent;
        public static event Action ForwardBtnClickEvent;
        public static event Action BackBtnClickEvent;

        [SerializeField]
        private Button _playBtn;

        [SerializeField]
        private Button _pauseBtn;

        [SerializeField]
        private Button _speedUpBtn;

        [SerializeField]
        private Button _speedDownBtn;

        [SerializeField]
        private Button _forwardBtn;

        [SerializeField]
        private Button _backBtn;

        [SerializeField]
        private PassedDistanceController _passedDistanceController;

        private void OnEnable()
        {
            SubscribeOnEvents();
        }

        private void OnDisable()
        {
            UnsubscribeFromEvents();
        }

        private void SubscribeOnEvents()
        {
            _playBtn.onClick.AddListener(Play);
            _pauseBtn.onClick.AddListener(Pause);
            _speedUpBtn.onClick.AddListener(SpeedUp);
            _speedDownBtn.onClick.AddListener(SpeedDown);
            _forwardBtn.onClick.AddListener(Forward);
            _backBtn.onClick.AddListener(Back);
            LabEqualizer.GetPassedDistance += SetPassedDistance;
        }

        private void UnsubscribeFromEvents()
        {
            _playBtn.onClick.RemoveListener(Play);
            _pauseBtn.onClick.RemoveListener(Pause);
            _speedUpBtn.onClick.RemoveListener(SpeedUp);
            _speedDownBtn.onClick.AddListener(SpeedDown);
            _forwardBtn.onClick.RemoveListener(Forward);
            _backBtn.onClick.RemoveListener(Back);
            LabEqualizer.GetPassedDistance -= SetPassedDistance;
        }

        private void Play()
        {
            _playBtn.gameObject.SetActive(false);
            _pauseBtn.gameObject.SetActive(true);
            PlayBtnClickEvent?.Invoke();
        }

        private void Pause()
        {
            _playBtn.gameObject.SetActive(true);
            _pauseBtn.gameObject.SetActive(false);
            PauseBtnClickEvent?.Invoke();
        }

        private void SpeedUp()
        {
            _speedDownBtn.gameObject.SetActive(true);
            _speedUpBtn.gameObject.SetActive(false);
            SpeedUpBtnClickEvent?.Invoke();
        }

        private void SpeedDown()
        {
            _speedDownBtn.gameObject.SetActive(false);
            _speedUpBtn.gameObject.SetActive(true);
            SpeedDownBtnClickEvent?.Invoke();
        }

        private void Forward()
        {
            ForwardBtnClickEvent?.Invoke();
        }

        private void Back()
        {
            BackBtnClickEvent?.Invoke();
        }

        private void SetPassedDistance(float value)
        {
            if (!_passedDistanceController.Slide)
            {
                _passedDistanceController.SetSliderValue(value);
            }
        }
    }
}