using System;
using UnityEngine;

namespace Assets.Scripts.MonoBehaviours.Controllers
{
    public class LabEqualizer : MonoBehaviour
    {
        public static event Action<float> GetPassedDistance;

        private const float SPEED_MODIFIER = 1.25f;
        private const float NORMAL_SPEED = 1f;
        private const float SECONDS_REWIND = 10;

        private Animator _animator;
        private AudioSource _audioSource;

        private float _currentSpeed = 1;

        private void Awake()
        {
            _animator = GetComponent<Animator>();
            _audioSource = GetComponent<AudioSource>();
        }

        private void LateUpdate()
        {
            GetPassedDistance?.Invoke(_animator.GetCurrentAnimatorStateInfo(0).normalizedTime);
        }

        private void OnEnable()
        {
            SubscribeOnEvents();
        }

        private void OnDisable()
        {
            UnsubscribeFromEvents();
        }

        private void SubscribeOnEvents()
        {
            ControllButtons.PlayBtnClickEvent += Play;
            ControllButtons.PauseBtnClickEvent += Pause;
            ControllButtons.SpeedUpBtnClickEvent += SpeedUp;
            ControllButtons.SpeedDownBtnClickEvent += SpeedDown;
            ControllButtons.ForwardBtnClickEvent += Forward;
            ControllButtons.BackBtnClickEvent += Back;
            PassedDistanceController.SliderChangedValueEvent += SetNewTimeBySlider;
        }

        private void UnsubscribeFromEvents()
        {
            ControllButtons.PlayBtnClickEvent -= Play;
            ControllButtons.PauseBtnClickEvent -= Pause;
            ControllButtons.SpeedUpBtnClickEvent -= SpeedUp;
            ControllButtons.SpeedDownBtnClickEvent -= SpeedDown;
            ControllButtons.ForwardBtnClickEvent -= Forward;
            ControllButtons.BackBtnClickEvent -= Back;
            PassedDistanceController.SliderChangedValueEvent -= SetNewTimeBySlider;
        }

        private void Play()
        { 
            _animator.speed = _currentSpeed;
            _audioSource.UnPause();
        }

        private void Pause()
        {
            _animator.speed = 0;
            _audioSource.Pause();
        }

        private void SpeedUp()
        {
            _animator.speed = SPEED_MODIFIER;
            _audioSource.pitch = SPEED_MODIFIER;
            _currentSpeed = SPEED_MODIFIER;
        }

        private void SpeedDown()
        {
            _animator.speed = NORMAL_SPEED;
            _audioSource.pitch = NORMAL_SPEED;
            _currentSpeed = NORMAL_SPEED;
        }

        private void Forward()
        {
            var newAudioTime = GetCurrentAudioTime() + SECONDS_REWIND;
            _audioSource.time = Mathf.Clamp(newAudioTime, 0, _audioSource.clip.length - 0.1f);
            _audioSource.Play();

            var newAnimTime = _animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
            _animator.Play(_animator.GetCurrentAnimatorClipInfo(0)[0].clip.name, 0, Mathf.Clamp(newAnimTime + SECONDS_REWIND / _audioSource.clip.length, 0, 1));
        }

        private void Back()
        {
            var newAudioTime = GetCurrentAudioTime() - SECONDS_REWIND;
            _audioSource.time = Mathf.Clamp(newAudioTime, 0, _audioSource.clip.length);
            _audioSource.Play();

            var newAnimTime = _animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
            _animator.Play(_animator.GetCurrentAnimatorClipInfo(0)[0].clip.name, 0, Mathf.Clamp(newAnimTime - SECONDS_REWIND / _audioSource.clip.length, 0, 1));
        }

        private void SetNewTimeBySlider(float value)
        {
            _audioSource.time = value * _audioSource.clip.length;
            _audioSource.Play();

            _animator.Play(_animator.GetCurrentAnimatorClipInfo(0)[0].clip.name, 0, value);
        }

        private float GetCurrentAudioTime()
        {
            return _animator.GetCurrentAnimatorStateInfo(0).normalizedTime * _audioSource.clip.length;
        }
    }
}